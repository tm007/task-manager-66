package ru.tsc.apozdnov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public interface DateUtil {

    @NotNull
    String PATTERN = "MM-DD-YYYY";

    @NotNull
    SimpleDateFormat FORMATTER = new SimpleDateFormat(PATTERN);

    @Nullable
    static Date toDate(final String value) {
        try {
            return FORMATTER.parse(value);
        } catch (@NotNull final ParseException e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    @NotNull
    static String toString(final Date value) {
        if (value == null) return "";
        return FORMATTER.format(value);
    }

}