package ru.tsc.apozdnov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.apozdnov.tm.api.ProjectRestEndpoint;
import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.service.ProjectService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/project")
public class ProjectRestEndpointImpl implements ProjectRestEndpoint {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Override
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return new ArrayList<>(projectService.findAll());
    }

    @Override
    @GetMapping("/findById/{id}")
    public Project findById(@NotNull @PathVariable("id") final String id) {
        return projectService.findById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return projectService.existsById(id);
    }

    @Override
    @PostMapping("/save")
    public Project save(@NotNull @RequestBody final Project project) {
        return projectService.add(project);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@NotNull @RequestBody final Project project) {
        projectService.remove(project);
    }

    @Override
    @PostMapping("/deleteAll")
    public void clear(@NotNull @RequestBody final List<Project> projects) {
        projectService.remove(projects);
    }

    @Override
    @DeleteMapping("/clear")
    public void clear() {
        projectService.clear();
    }

    @Override
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@NotNull @PathVariable("id") final String id) {
        projectService.removeById(id);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return projectService.count();
    }

}