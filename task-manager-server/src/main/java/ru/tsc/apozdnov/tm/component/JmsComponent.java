package ru.tsc.apozdnov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.apozdnov.tm.api.service.IServiceJMS;
import ru.tsc.apozdnov.tm.dto.LogDto;
import ru.tsc.apozdnov.tm.service.JmsService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class JmsComponent {

    private static final int THREAD_COUNT = 3;

    @NotNull
    private final ExecutorService es = Executors.newFixedThreadPool(THREAD_COUNT);

    @NotNull
    @Autowired
    private IServiceJMS service;

    public void sendMessage(@NotNull final Object object, @NotNull final String type) {
        es.submit(() -> {
            @NotNull final LogDto logDto = service.createMessage(object, type);
            service.send(logDto);
        });
    }

    public void stop() {
        es.shutdown();
    }

}