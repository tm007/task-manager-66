package ru.tsc.apozdnov.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.apozdnov.tm.dto.model.AbstractDtoModel;


@Repository
public interface AbstractDtoRepository<M extends AbstractDtoModel> extends JpaRepository<M, String> {


}