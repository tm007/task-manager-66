package ru.tsc.apozdnov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.apozdnov.tm.api.service.dto.ITaskDtoService;
import ru.tsc.apozdnov.tm.api.service.dto.IUserDtoService;
import ru.tsc.apozdnov.tm.config.ContextConfig;
import ru.tsc.apozdnov.tm.dto.model.TaskDtoModel;
import ru.tsc.apozdnov.tm.dto.model.UserDtoModel;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.exception.field.EmptyDescriptionException;
import ru.tsc.apozdnov.tm.exception.field.EmptyIdException;
import ru.tsc.apozdnov.tm.exception.field.EmptyNameException;
import ru.tsc.apozdnov.tm.exception.field.EmptyUserIdException;
import ru.tsc.apozdnov.tm.util.DateUtil;

import java.util.List;
import java.util.UUID;

public class TaskServiceTest {

    @NotNull
    private IUserDtoService userService;

    @NotNull
    private ITaskDtoService taskService;

    private String USER_ID;

    private String TASK_ID;

    private long INITIAL_SIZE;

    @Before
    public void init() {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ContextConfig.class);
        userService = context.getBean(IUserDtoService.class);
        taskService = context.getBean(ITaskDtoService.class);
        @NotNull final UserDtoModel user = userService.create("user", "user");
        USER_ID = user.getId();
        @NotNull final TaskDtoModel task = taskService.create(USER_ID, "test-1");
        TASK_ID = task.getId();
        INITIAL_SIZE = taskService.getCount();
    }

    @After
    public void end() {
        taskService.clear(USER_ID);
        userService.removeByLogin("user");
    }

    @Test
    public void create() {
        Assert.assertThrows(EmptyUserIdException.class, () -> taskService.create("", "test"));
        Assert.assertThrows(EmptyNameException.class, () -> taskService.create(USER_ID, ""));
        taskService.create(USER_ID, "test");
        Assert.assertEquals(INITIAL_SIZE + 1, taskService.getCount());
    }

    @Test
    public void createWithDescription() {
        Assert.assertThrows(EmptyUserIdException.class, () -> taskService.create("", "test", "test"));
        Assert.assertThrows(EmptyNameException.class, () -> taskService.create(USER_ID, "", "test"));
        Assert.assertThrows(EmptyDescriptionException.class, () -> taskService.create(USER_ID, "test", ""));
        taskService.create(USER_ID, "test", "test");
        Assert.assertEquals(INITIAL_SIZE + 1, taskService.getCount());
    }

    @Test
    public void createWithDescriptionAndDate() {
        @Nullable final TaskDtoModel task = taskService.create(
                USER_ID,
                "test",
                "test",
                DateUtil.toDate("10.10.2021"),
                DateUtil.toDate("11.11.2021")
        );
        Assert.assertEquals(INITIAL_SIZE + 1, taskService.getCount());
        Assert.assertNotNull(task.getDateBegin());
        Assert.assertNotNull(task.getDateEnd());
    }

    @Test
    public void clear() {
        taskService.clear();
        Assert.assertEquals(0, taskService.getCount());
    }

    @Test
    public void findAll() {
        @NotNull final List<TaskDtoModel> tasksAll = taskService.findAll();
        Assert.assertEquals(INITIAL_SIZE, tasksAll.size());
        @NotNull final List<TaskDtoModel> tasksOwnedUser1 = taskService.findAll(USER_ID);
        Assert.assertEquals(1, tasksOwnedUser1.size());
        @NotNull final List<TaskDtoModel> tasksOwnedUser2 = taskService.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(0, tasksOwnedUser2.size());
    }

    @Test
    public void updateById() {
        Assert.assertThrows(EmptyUserIdException.class,
                () -> taskService.updateById("", TASK_ID, "test", "test"));
        Assert.assertThrows(EmptyIdException.class,
                () -> taskService.updateById(USER_ID, "", "test", "test"));
        Assert.assertThrows(EmptyNameException.class,
                () -> taskService.updateById(USER_ID, TASK_ID, "", "test"));
        @NotNull final String newName = "new name";
        @NotNull final String newDescription = "new description";
        taskService.updateById(USER_ID, TASK_ID, newName, newDescription);
        @Nullable final TaskDtoModel task = taskService.findOneById(TASK_ID);
        Assert.assertEquals(newName, task.getName());
        Assert.assertEquals(newDescription, task.getDescription());
    }

    @Test
    public void changeTaskStatusById() {
        @NotNull final Status newStatus = Status.COMPLETED;
        Assert.assertThrows(EmptyUserIdException.class,
                () -> taskService.changeStatusById("", TASK_ID, newStatus));
        Assert.assertThrows(EmptyIdException.class,
                () -> taskService.changeStatusById(USER_ID, "", newStatus));
        taskService.changeStatusById(USER_ID, TASK_ID, newStatus);
        @Nullable final TaskDtoModel task = taskService.findOneById(TASK_ID);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(newStatus, task.getStatus());
    }

    @Test
    public void findOneById() {
        @NotNull final String taskName = "test find by id";
        @NotNull final TaskDtoModel task = taskService.create(USER_ID, taskName);
        @NotNull final String taskId = task.getId();
        Assert.assertThrows(EmptyIdException.class, () -> taskService.findOneById(""));
        Assert.assertNotNull(taskService.findOneById(taskId));
        Assert.assertEquals(taskName, taskService.findOneById(taskId).getName());
        Assert.assertNotNull(taskService.findOneById(USER_ID, taskId));
        Assert.assertEquals(taskName, taskService.findOneById(USER_ID, taskId).getName());
    }

    @Test
    public void existsById() {
        @NotNull final String taskName = "test exist by id";
        @NotNull final TaskDtoModel task = taskService.create(USER_ID, taskName);
        @NotNull final String taskId = task.getId();
        Assert.assertTrue(taskService.existsById(taskId));
        Assert.assertFalse(taskService.existsById(UUID.randomUUID().toString()));
    }

    @Test
    @Ignore
    public void remove() {
        @NotNull final TaskDtoModel task = taskService.create(USER_ID, "test");
        @NotNull final String taskId = task.getId();
        taskService.remove(task);
        Assert.assertEquals(INITIAL_SIZE, taskService.getCount());
        taskService.add(task);
        taskService.remove(USER_ID, task);
        Assert.assertEquals(INITIAL_SIZE, taskService.getCount());
    }

    @Test
    public void removeById() {
        @NotNull final TaskDtoModel task = taskService.create(USER_ID, "test");
        @NotNull final String taskId = task.getId();
        Assert.assertThrows(EmptyIdException.class, () -> taskService.removeById(""));
        taskService.removeById(taskId);
        Assert.assertEquals(INITIAL_SIZE, taskService.getCount());
        taskService.add(task);
        Assert.assertThrows(EmptyIdException.class, () -> taskService.removeById(USER_ID, ""));
        taskService.removeById(USER_ID, taskId);
        Assert.assertEquals(INITIAL_SIZE, taskService.getCount());
    }

}