package ru.tsc.apozdnov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

public interface IEndpoint {

    @NotNull
    String NAMESPACE = "http://endpoint.tm.apozdnov.tsc.ru/";

    @NotNull
    String REQUEST = "request";

    @SneakyThrows
    @WebMethod(exclude = true)
    static <T> T newInstance(
            @NotNull final String host,
            @NotNull final String port,
            @NotNull final String name,
            @NotNull final String namespace,
            @NotNull final String part,
            @NotNull final Class<T> clazz
    ) {
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(namespace, part);
        return Service.create(url, qName).getPort(clazz);
    }

}