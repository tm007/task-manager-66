package ru.tsc.apozdnov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.apozdnov.tm.event.ConsoleEvent;
import ru.tsc.apozdnov.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class ArgumentsListener extends AbstractSystemListCommandListener {

    @NotNull
    public static final String NAME = "arguments";

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    public static final String DESCRIPTION = "Show argument list.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@argumentsListener.getName() == #event.name || @argumentsListener.getArgument() == #event.name")
    public void executeEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractListener> commands = getArguments();
        for (final AbstractListener command : commands) {
            @Nullable final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}